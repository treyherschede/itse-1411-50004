var app = angular.module("myApp", []); //defines the module

app.controller("myCtrl", function($scope) {
	$scope.shape; //1-rectangle, 2-circle, 3-prism, 4-cylinder, 5-sphere
	$scope.length = 1;
	$scope.width = 1;
	$scope.height = 1;
	$scope.radius = 1;
	$scope.pi = Math.PI;
	$scope.setFormat = function(num) {
		return num.toFixed(2);
	} //end setFormat()
}); //end controller myCtrl

app.directive("showHeader", function() {
	return {
		template:
			"Select a shape: " +
			"<select ng-model='shape'>" +
			"  <option value='1' selected>Rectangle</option>" +
			"  <option value='2' selected>Circle</option>" +
			"  <option value='3' selected>Prism</option>" +
			"  <option value='4' selected>Cylinder</option>" +
			"  <option value='5' selected>Sphere</option>" +
			"</select>"
	} //end return
}); //end directive showHeader

app.directive("showShape", function() {
	return {
		template:
			"<hr>" +
			"<div ng-switch='shape'>" +
			"  <div ng-switch-when='1'>" +
			"    <h3>Rectangle</h3>" +
			"    <p>Length: <input type='text' ng-model='length'></p>" +
			"    <p>Width: <input type='text' ng-model='width'></p>" +
			"    <p>Perimeter of rectangle is {{setFormat((2 * length) + (2 * width))}}</p>" +
			"    <p>Area of rectangle is {{setFormat(length * width)}}</p>" +
			"  </div>" +
			"  <div ng-switch-when='2'>" +
			"    <h3>Circle</h3>" +
			"    <p>Radius: <input type='text' ng-model='radius'></p>" +
			"    <p>Perimeter of circle is {{setFormat(2 * pi * radius)}}</p>" +
			"    <p>Area of circle is {{setFormat(pi * radius * radius)}}</p>" +
			"  </div>" +
			"  <div ng-switch-when='3'>" +
			"    <h3>Prism</h3>" +
			"    <p>Length: <input type='text' ng-model='length'></p>" +
			"    <p>Width: <input type='text' ng-model='width'></p>" +
			"    <p>Height: <input type='text' ng-model='height'></p>" +
			"    <p>Surface area of prism is {{setFormat((2 * length * width) + (2 * length * height) + (2 * width * height))}}</p>" +
			"    <p>Volume of prism is {{setFormat(length * width * height)}}</p>" +
			"  </div>" +
			"  <div ng-switch-when='4'>" +
			"    <h3>Cylinder</h3>" +
			"    <p>Radius: <input type='text' ng-model='radius'></p>" +
			"    <p>Height: <input type='text' ng-model='height'></p>" +
			"    <p>Surface area of cylinder is {{setFormat((2 * pi * radius * height) + (2 * pi * radius * radius))}}</p>" +
			"    <p>Volume of cylinder is {{setFormat(pi * radius * radius * height)}}</p>" +
			"  </div>" +
			"  <div ng-switch-when='5'>" +
			"    <h3>Sphere</h3>" +
			"    <p>Radius: <input type='text' ng-model='radius'></p>" +
			"    <p>Surface area of sphere is {{setFormat(4 * pi * radius * radius)}}</p>" +
			"    <p>Volume of sphere is {{setFormat(4 * pi * radius * radius * radius / 3)}}</p>" +
			"  </div>" +
			"  <div ng-switch-default>" +
			"    <h3>Select shape from the dropdown to switch the content.</h3>" +
			"  </div>" +
			"</div>" +
			"<hr>"
	} //end return
}); //end directive showShape

/* app.directive("showRectangle", function() {
	return {
		template:
			"<div ng-show='{{shape}} == 1'>" +
			"<h3>Rectangle</h3>" +
			"<p>Length: <input type='text' ng-model='length'></p>" +
			"<p>Width: <input type='text' ng-model='width'></p>" +
			"<p>Perimeter of rectangle is {{(2 * length) + (2 * width)}}</p>" +
			"<p>Area of rectangle is {{length * width}}</p>" +
			"</div>"
	} //end return
}); //end directive showRectangle */

/* app.directive("showCircle", function() {
	return {
		template:
			"<div ng-show='{{shape}} == 2'>" +
			"<h3>Circle</h3>" +
			"<p>Radius: <input type='text' ng-model='radius'></p>" +
			"<p>Perimeter of circle is {{2 * pi * radius}}</p>" +
			"<p>Area of circle is {{pi * radius * radius}}</p>" +
			"</div>"
	} //end return
}); //end directive showCircle */

/* app.directive("showPrism", function() {
	return {
		template:
			"<div ng-show='{{shape}} == 3'>" +
			"<h3>Prism</h3>" +
			"<p>Length: <input type='text' ng-model='length'></p>" +
			"<p>Width: <input type='text' ng-model='width'></p>" +
			"<p>Height: <input type='text' ng-model='height'></p>" +
			"<p>Surface area of prism is {{(2 * length * width) + (2 * length * height) + (2 * width * height)}}</p>" +
			"<p>Volume of prism is {{length * width * height}}</p>" +
			"</div>"
	} //end return
}); //end directive showCircle */

/* app.directive("showCylinder", function() {
	return {
		template:
			"<div ng-show='{{shape}} == 4'>" +
			"<h3>Cylinder</h3>" +
			"<p>Radius: <input type='text' ng-model='radius'></p>" +
			"<p>Height: <input type='text' ng-model='height'></p>" +
			"<p>Surface area of cylinder is {{(2 * pi * radius * height) + (2 * pi * radius * radius)}}}</p>" +
			"<p>Volume of cylinder is {{pi * radius * radius * height}}</p>" +
			"</div>"
	} //end return
}); //end directive showCircle */

/* app.directive("showSphere", function() {
	return {
		template:
			"<div ng-show='{{shape}} == 5'>" +
			"<h3>Sphere</h3>" +
			"<p>Radius: <input type='text' ng-model='radius'></p>" +
			"<p>Surface area of sphere is {{4 * pi * radius * radius}}</p>" +
			"<p>Volume of sphere is {{4 * pi * radius * radius * radius / 3}}</p>" +
			"</div>"
	} //end return
}); //end directive showCircle */

/* //define directives
app.directive("myDirective", function() {
	return {
		template : "<p>The perimeter of the rectangle is {{(2 * width) + (2 * height)}}</p>"
	}
}); */

/* //directive for showing inputs (varies by shape)
app.directive("shapeInputs", function() {
	return {
		controller: function($scope, $element) {
			var shape = $scope.shape;
			switch (shape) {
				case 1:
					title = "Rectangle";
					lbl1 = "Length: ";
					lbl2 = "Width: ";
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				default:
			}
		},
		template: 
			"<h3>{{title}}</h3>" +
			"<p>{{lbl1}}<input type='text' ng-model='x1'></p>" +
			"<p>{{lbl2}}<input type='text' ng-model='x2'></p>"	
	}
}); */

//directive for showing results (varies by shape)